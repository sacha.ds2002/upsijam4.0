using UnityEngine;

[CreateAssetMenu(fileName = "ItemData", menuName = "Game/Item Data")]
public class ItemData : ScriptableObject
{
	public string ItemId;
	public string ItemKey;
	public string ItemSpritePath;
}
