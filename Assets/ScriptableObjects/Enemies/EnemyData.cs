using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "Game/Enemy Data")]
public class EnemyData : ScriptableObject 
{
	public string EnemyId;
	public string EnemyItem;
	public string EnemySpritePath;
	public string EnemyVoiceline1;
	public string EnemyVoiceline2;
}
