using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapForward : MonoBehaviour
{
	private Tween t;

	private void Start()
	{
		CreateMove();
	}

	private void CreateMove()
	{
		Vector3 endPos = new(0f, -20f, 0f);
		t = transform.DOMove(endPos, 12f).SetEase(Ease.Linear).OnComplete(
			() => ResetTransform()
		);
	}

	private void ResetTransform()
    {
		transform.Translate(0f, 20f, 0f);
		t.Kill();
		CreateMove();
    }
}
