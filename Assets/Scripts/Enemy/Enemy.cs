using DG.Tweening;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Tween t;

    public string key;

    public bool IsSatisfied { get; private set; }

    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private EnemyData enemyData;

    public void InitializeEnemy(EnemyData enemyData)
    {
        this.enemyData = enemyData;
        spriteRenderer.sprite = Resources.Load<Sprite>(enemyData.EnemySpritePath);
        t = this.transform.DOMove(GameManager.I.Player.transform.position, GameManager.I.enemySpeed).SetEase(Ease.Linear).OnComplete(
            () => PlayerHit()
        );
    }

    //void Update()
    // {    
    //     if (Input.GetKeyDown(key))
    //     {
    //         Debug.Log("proper key pressed " + key);
    //GameManager.I.LaunchObject(key);
    //         t.Kill();
    //     }
    // }

    public void EnemyCheckSatisfaction(string itemId)
    {
        t.Kill();
        if (itemId == enemyData.EnemyItem)
        {
            IsSatisfied = true;
            Debug.Log("enemy death");
            GameManager.I.AddPoints(100);
            GameManager.I.NextEnemy();
        } else
            PlayerHit();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Item"))
            collision.GetComponent<ThrownItem>().ThrowEnd();
    }

    void PlayerHit()
    {
        IsSatisfied = false;
        GameManager.I.UpdateHealth(-1);
        GameManager.I.NextEnemy();
    }
}
