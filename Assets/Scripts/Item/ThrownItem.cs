using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Unity.VisualScripting;

public class ThrownItem : MonoBehaviour
{
	[SerializeField] private ItemData itemData;
    [SerializeField] private SpriteRenderer spriteRenderer;

    public bool sent = false;

    public void CreateItem(ItemData itemData)
    {
        if (itemData == null)
            Debug.LogError("Item data not valid");
        else
        {
			this.itemData = itemData;
			spriteRenderer.sprite = Resources.Load<Sprite>(itemData.ItemSpritePath);
		}
    }

	private void FixedUpdate()
	{
		if (sent)
		{
			transform.Translate(0f, 0.2f, 0f);
			if (transform.position.y > 20)
				Destroy(gameObject);
		}
	}

	public void ThrowEnd()
    {
        GameManager.I.Enemy.GetComponent<Enemy>().EnemyCheckSatisfaction(itemData.ItemId);
        Destroy(gameObject);
	}
}
