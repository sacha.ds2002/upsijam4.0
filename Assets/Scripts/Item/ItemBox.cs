using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemBox : MonoBehaviour
{
    [SerializeField] private ItemData itemData;
	[SerializeField] private Image image;
	[SerializeField] private TMP_Text key;

	public void Start()
	{
		if (itemData != null)
		{
			image.sprite = Resources.Load<Sprite>(itemData.ItemSpritePath);
			key.text = itemData.ItemKey;
		}
	}
}
