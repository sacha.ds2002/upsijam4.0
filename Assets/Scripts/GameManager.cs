using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private int health;
    private int points;

    [SerializeField] private int MaxHealth;
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private HPController hpController;
    [SerializeField] private TMP_Text pointsText;
    [SerializeField] private List<EnemyData> enemiesData;
    [SerializeField] private List<ItemData> itemsData;
    [SerializeField] private ThrownItem itemPrefab;
    [SerializeField] private GameObject difficultyPanel;


    public float enemySpeed;
    public GameObject Player;
    public Transform SpawnPoint;
    public Transform ItemSpawnPoint;
    public GameObject enemyPrefab;
    public GameObject Enemy;
    public List<Enemy> Enemies;
    public int id;
    public AudioSource Audio;

    #region SINGLETON 

    private static GameManager instance;
    private static readonly object padlock = new();


    private GameManager()
    {
    }

    public static GameManager I
    {
        get
        {
            lock (padlock)
            {
                return instance;
            }
        }
    }

    #endregion

    #region PROPERTIES

    public int Health
    {
        set { health = value; }
        get { return health; }
    }

    public int Points
    {
        set { points = value; }
        get { return points; }
    }

    #endregion

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
        StartGame();
    }

    private void Start()
    {
        Audio = Instantiate(Audio, transform.position, Quaternion.identity);
    }

    #region HP AND POINTS

    public void UpdateHealth(int difference)
    {
        if (health + difference > MaxHealth)
            health = MaxHealth;
        else if (health + difference < 0)
            health = 0;
        else
            health += difference;
        hpController.RenderHealth(health);
    }

    public void AddPoints(int points)
    {
        Points += points;
        if (Points % 1000 == 0 && enemySpeed > 1.4f)
        {
            enemySpeed -= 0.4f;
            StartCoroutine(DifficultyShow());
        }
        pointsText.text = Points.ToString();
    }

    private IEnumerator DifficultyShow()
    {
        for (int i = 0; i < 6; i++)
        {
            if (i % 2 == 0)
                difficultyPanel.SetActive(true);
            else
                difficultyPanel.SetActive(false);
            yield return new WaitForSeconds(0.3f);
        }
    }

    #endregion

    #region GAME MANAGEMENT

    public void StartGame()
    {
        health = MaxHealth;
        points = 0;
        hpController.RenderHealth(health);
        id = 0;
        InstanciateEnemy();
    }

    public void PauseGame()
    {
        if (Time.fixedDeltaTime == 0)
            ResumeGame();
        Time.fixedDeltaTime = 0;
        pausePanel.SetActive(true);
    }

    public void ResumeGame()
    {
        Time.fixedDeltaTime = 1;
        pausePanel.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    #endregion

    #region ENEMY MANAGEMENT

    public void InstanciateEnemy()
    {
        if (health <= 0)
        {
            GlobalGameManager.I.Score = points;
            GlobalGameManager.I.LoadSceneLevel(3);
        }
        id = Random.Range(0, enemiesData.Count);
        Enemy = Instantiate(enemyPrefab, SpawnPoint);
        Enemy.GetComponent<Enemy>().InitializeEnemy(enemiesData[id]);
    }

    #endregion

    private ItemData ParseKey(string key)
    {
        switch (key)
        {
            case "a":
                return itemsData[0];
            case "s":
                return itemsData[1];
            case "d":
                return itemsData[2];
            case "f":
                return itemsData[3];
            case "h":
                return itemsData[4];
            case "j":
                return itemsData[5];
            case "k":
                return itemsData[6];
            case "l":
                return itemsData[7];
            default:
                Debug.LogError("Item not found");
                break;
        }
        return null;
    }

    public ThrownItem CreateItem(string key)
    {
        Debug.Log(key);
        itemPrefab.CreateItem(ParseKey(key));
        return Instantiate(itemPrefab, ItemSpawnPoint).GetComponent<ThrownItem>();
    }

    public void LaunchObject(string key)
    {
        ThrownItem newItem = CreateItem(key);
        newItem.sent = true;
    }

    public void NextEnemy()
    {
        if (!Enemy.GetComponent<Enemy>().IsSatisfied)
        {
            AudioClip sound = Resources.Load<AudioClip>(enemiesData[id].EnemyVoiceline1);
            Audio.PlayOneShot(sound);
        }
        Destroy(Enemy);
        InstanciateEnemy();
    }
}
