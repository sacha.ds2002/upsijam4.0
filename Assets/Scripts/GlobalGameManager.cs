using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalGameManager : MonoBehaviour
{
	[SerializeField] private GameObject crossfade;
	[SerializeField] private Animator transition;
	[SerializeField] private float transitionTime = 0.6f;

	public int Score = 0;

	#region SINGLETON 

	private static GlobalGameManager instance;
	private static readonly object padlock = new();


	private GlobalGameManager()
	{
	}

	public static GlobalGameManager I
	{
		get
		{
			lock (padlock)
			{
				return instance;
			}
		}
	}

	#endregion

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
			Destroy(gameObject);
	}

	public void LoadSceneLevel(int lvl)
	{
		StartCoroutine(LoadLevel(lvl));
	}

	IEnumerator LoadLevel(int levelIndex)
	{
		crossfade.SetActive(true);
		transition.SetTrigger("StartCrossfade");

		yield return new WaitForSeconds(transitionTime);

		SceneManager.LoadScene(levelIndex, LoadSceneMode.Single);
		transition.SetTrigger("EndCrossfade");

		yield return new WaitForSeconds(transitionTime);

		crossfade.SetActive(false);
	}
}
