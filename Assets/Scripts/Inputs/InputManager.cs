using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
	private List<ItemData> itemList;

	private GeneralInputs generalInputs;
	private InputAction escape;
	private InputAction throwItem;

	private void Awake()
	{
		generalInputs = new GeneralInputs();
	}

	private void OnEnable()
	{
		escape = generalInputs.Player.Escape;
		escape.performed += Escape;
		escape.Enable();
		throwItem = generalInputs.Player.ThrowItem;
		throwItem.performed += ThrowItem;
		throwItem.Enable();
	}

	private void OnDisable()
	{
		escape.Disable();
		throwItem.Disable();
	}

	private void Escape(InputAction.CallbackContext context)
	{
		if (context.performed)
			GameManager.I.PauseGame();
	}

	private void ThrowItem(InputAction.CallbackContext context)
	{
		if (context.performed)
			GameManager.I.LaunchObject(context.control.name);
	}

	public void InitializeInput(List<ItemData> itemList)
	{
		this.itemList = itemList;
	}
}
