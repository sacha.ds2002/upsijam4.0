using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverController : MonoBehaviour
{
    public Sprite b0;
    public Sprite b1;
    public Sprite b2;
    public Sprite b3;
    public Image background;

	public void Start()
	{
        int score = GameManager.I.Points;
        if (score == 0)
            background.sprite = b0;
        else if (score < 1000)
            background.sprite = b1;
        else if (score < 2000)
            background.sprite = b2;
        else
            background.sprite = b3;
	}

	public void Retry()
    {
        GlobalGameManager.I.LoadSceneLevel(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
