using System.Collections.Generic;
using UnityEngine;

public class HPController : MonoBehaviour
{
    [SerializeField] private List<GameObject> hps;

    public void RenderHealth(int health)
    {
        foreach (GameObject hp in hps)
        {
            if (health-- > 0)
                hp.SetActive(true);
            else
                hp.SetActive(false);
        }
    }
}
