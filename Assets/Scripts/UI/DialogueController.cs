using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class DialogueController : MonoBehaviour
{
	[SerializeField, TextArea(3,5)] private List<string> sentences;
	//[SerializeField] private Queue<string> sentences;
	public TMP_Text dialogueText;
	private int index;

	private GeneralInputs generalInputs;
	private InputAction skipDialogue;

	private void Awake()
	{
		generalInputs = new GeneralInputs();
	}

	private void Start()
	{
		StartDialogue();
	}

	private void OnEnable()
	{
		skipDialogue = generalInputs.Player.SkipDialogue;
		skipDialogue.performed += SkipDialogue;
		skipDialogue.Enable();
	}

	private void SkipDialogue(InputAction.CallbackContext context)
	{
		if (context.performed)
			DisplayNextSentence();
	}

	private void OnDisable()
	{
		skipDialogue.Disable();
	}

	public void StartDialogue()
	{
		index = -1;
		DisplayNextSentence();
	}

	public void DisplayNextSentence()
	{
		index++;
		if (sentences.Count <= index)
		{
			EndDialogue();
			return;
		}
		StopAllCoroutines();
		StartCoroutine(TypeSentence(sentences[index]));
	}

	IEnumerator TypeSentence(string sentence)
	{
		dialogueText.text = "";
		foreach (char letter in sentence.ToCharArray())
		{
			dialogueText.text += letter;
			yield return new WaitForSeconds(0.01f);
		}
	}

	private void EndDialogue()
	{
		GlobalGameManager.I.LoadSceneLevel(1);
	}
}
